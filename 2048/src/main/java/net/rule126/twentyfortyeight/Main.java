package net.rule126.twentyfortyeight;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.mdp.core.action.Action;
import burlap.mdp.singleagent.SADomain;
import burlap.mdp.singleagent.model.FactoredModel;
import burlap.statehashing.simple.SimpleHashableStateFactory;

public class Main {
    public static void main(String[] args) {
        final int WIDTH = 3;
        final int HEIGHT = 3;

        SADomain domain = new SADomain();
        domain.addActionType(new BoardActionType());
        BoardFullStateModel boardFullStateModel = new BoardFullStateModel();
        BoardRewardFunction boardRewardFunction = new BoardRewardFunction();
        domain.setModel(
                new FactoredModel(
                        boardFullStateModel,
                        boardRewardFunction,
                        new BoardTerminalFunction()
                )
        );

        ValueIteration planner = new ValueIteration(domain, 0.99, new SimpleHashableStateFactory(), 0, 10);
        int[] contents = new int[WIDTH * HEIGHT];
        contents[0] = 1;
        BoardState initialState = new BoardState(WIDTH, HEIGHT, contents);
        GreedyQPolicy policy = planner.planFromState(initialState);

        System.out.println(initialState);
        BoardState state = initialState;
        do {
            Action action = policy.action(state);
            System.out.println(action);
            state = boardFullStateModel.sample(state, action);
            System.out.println(state);
        } while (!state.isTerminal());
    }
}
