package net.rule126.twentyfortyeight;

import burlap.mdp.core.StateTransitionProb;
import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.model.statemodel.FullStateModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BoardFullStateModel extends BoardSampleStateModel implements FullStateModel {
    @Override
    public List<StateTransitionProb> stateTransitions(State state, Action action) {
        BoardState boardState = (BoardState) state;
        BoardAction boardAction = (BoardAction) action;

        int[] slidContents = boardAction.slide(boardState);
        assert boardState.isDifferent(slidContents);

        List<BoardState> boardStates = new ArrayList<>();
        for (int i = 0; i < slidContents.length; i++) {
            if (slidContents[i] == 0) {
                int[] modifiedContents = slidContents.clone();
                modifiedContents[i] = 1;
                boardStates.add(new BoardState(boardState.getWidth(), boardState.getHeight(), modifiedContents));
            }
        }
        return boardStates.stream().map(bs -> new StateTransitionProb(bs, 1.0 / boardStates.size())).collect(Collectors.toList());
    }
}
