package net.rule126.twentyfortyeight;

import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.model.RewardFunction;

public class BoardRewardFunction implements RewardFunction {
    @Override
    public double reward(State sourceState, Action action, State destinationState) {
        BoardState boardState = (BoardState) destinationState;
        return boardState.isWon() ? 1 : 0;
    }
}
