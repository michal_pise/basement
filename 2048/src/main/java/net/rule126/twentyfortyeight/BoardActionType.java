package net.rule126.twentyfortyeight;

import burlap.mdp.core.action.Action;
import burlap.mdp.core.action.ActionType;
import burlap.mdp.core.state.State;

import java.util.List;

import static net.rule126.twentyfortyeight.BoardAction.*;

public class BoardActionType implements ActionType {

    @Override
    public String typeName() {
        return this.getClass().getName();
    }

    @Override
    public burlap.mdp.core.action.Action associatedAction(String stringRepresentation) {
        switch (stringRepresentation) {
            case "LEFT":
                return MOVE_LEFT;
            case "RIGHT":
                return MOVE_RIGHT;
            case "UP":
                return MOVE_UP;
            case "DOWN":
                return MOVE_DOWN;
            default:
                throw new IllegalArgumentException("Unknown action '" + stringRepresentation + "'.");
        }
    }

    @Override
    public List<Action> allApplicableActions(State state) {
        return ((BoardState) state).allApplicableActions();
    }
}
