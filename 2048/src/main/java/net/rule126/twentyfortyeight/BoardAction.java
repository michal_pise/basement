package net.rule126.twentyfortyeight;

public enum BoardAction implements burlap.mdp.core.action.Action {
    MOVE_LEFT {
        @Override
        int indexOf(int offset, int columnNumber, BoardState boardState) {
            return offset + columnNumber * boardState.getWidth();
        }

        @Override
        int getColumnCount(BoardState boardState) {
            return boardState.getHeight();
        }

        @Override
        int getColumnLength(BoardState boardState) {
            return boardState.getWidth();
        }
    },
    MOVE_RIGHT {
        @Override
        int indexOf(int offset, int columnNumber, BoardState boardState) {
            return (boardState.getWidth() - 1 - offset) + columnNumber * boardState.getWidth();
        }

        @Override
        int getColumnCount(BoardState boardState) {
            return boardState.getHeight();
        }

        @Override
        int getColumnLength(BoardState boardState) {
            return boardState.getWidth();
        }
    },
    MOVE_UP {
        @Override
        int indexOf(int offset, int columnNumber, BoardState boardState) {
            return columnNumber + offset * boardState.getWidth();
        }

        @Override
        int getColumnCount(BoardState boardState) {
            return boardState.getWidth();
        }

        @Override
        int getColumnLength(BoardState boardState) {
            return boardState.getHeight();
        }
    },
    MOVE_DOWN {
        @Override
        int indexOf(int offset, int columnNumber, BoardState boardState) {
            return columnNumber + (boardState.getHeight() - 1 - offset) * boardState.getWidth();
        }

        @Override
        int getColumnCount(BoardState boardState) {
            return boardState.getWidth();
        }

        @Override
        int getColumnLength(BoardState boardState) {
            return boardState.getHeight();
        }
    };

    @Override
    public String actionName() {
        return this.toString();
    }

    @Override
    public burlap.mdp.core.action.Action copy() {
        return this;
    }

    abstract int indexOf(int offset, int columnNumber, BoardState boardState);

    abstract int getColumnCount(BoardState boardState);

    abstract int getColumnLength(BoardState boardState);

    public int[] slide(BoardState state) {
        int[] modifiedContents = state.getCopyOfContents();
        for (int columnNumber = 0; columnNumber < getColumnCount(state); columnNumber++) {
            int destinationOffset = 0;
            for (int sourceOffset = 0; sourceOffset < getColumnLength(state); ) {
                if (modifiedContents[indexOf(sourceOffset, columnNumber, state)] == 0) { // empty
                    sourceOffset++;
                } else if (sourceOffset < getColumnLength(state) - 1 &&
                        modifiedContents[indexOf(sourceOffset, columnNumber, state)] == modifiedContents[indexOf(sourceOffset + 1, columnNumber, state)]) { // double
                    int value = modifiedContents[indexOf(sourceOffset, columnNumber, state)] + 1;
                    modifiedContents[indexOf(sourceOffset, columnNumber, state)] = 0;
                    modifiedContents[indexOf(sourceOffset + 1, columnNumber, state)] = 0;
                    modifiedContents[indexOf(destinationOffset, columnNumber, state)] = value;
                    destinationOffset++;
                    sourceOffset += 2;
                } else { // single
                    int value = modifiedContents[indexOf(sourceOffset, columnNumber, state)];
                    modifiedContents[indexOf(sourceOffset, columnNumber, state)] = 0;
                    modifiedContents[indexOf(destinationOffset, columnNumber, state)] = value;
                    destinationOffset++;
                    sourceOffset++;
                }
            }
        }
        return modifiedContents;
    }
}
