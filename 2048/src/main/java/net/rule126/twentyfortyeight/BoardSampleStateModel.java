package net.rule126.twentyfortyeight;

import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.model.statemodel.SampleStateModel;

import java.util.Random;

public class BoardSampleStateModel implements SampleStateModel {
    private static final Random random = new Random(20170205);

    @Override
    public BoardState sample(State state, Action action) {
        BoardState boardState = (BoardState) state;
        BoardAction boardAction = (BoardAction) action;

        int[] modifiedContents = boardAction.slide(boardState);
        assert boardState.isDifferent(modifiedContents);

        do {
            int x = random.nextInt(boardState.getWidth());
            int y = random.nextInt(boardState.getHeight());
            if (modifiedContents[x + y * boardState.getWidth()] == 0) {
                modifiedContents[x + y * boardState.getWidth()] = 1;
                return new BoardState(boardState.getWidth(), boardState.getHeight(), modifiedContents);
            }
        } while (true);
    }
}
