package net.rule126.twentyfortyeight;

import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;

import static net.rule126.twentyfortyeight.BoardAction.*;

@SuppressWarnings("WeakerAccess")
public class BoardState implements State {
    public static final int WINNING_VALUE = 5;
    private static final ImmutableList<BoardAction> ALL_ACTIONS = ImmutableList.of(MOVE_LEFT, MOVE_RIGHT, MOVE_UP, MOVE_DOWN);

    private final int width;
    private final int height;
    private final int[] contents; // immutable

    public BoardState(int width, int height, int[] contents) {
        this.width = width;
        this.height = height;
        this.contents = contents;
    }

    @Override
    public List<Object> variableKeys() {
        return ImmutableList.of("this");
    }

    @Override
    public Object get(Object variableKey) {
        if ("this".equals(variableKey)) {
            return this;
        } else {
            throw new IllegalArgumentException("This should never happen.");
        }
    }

    @Override
    public State copy() {
        return new BoardState(width, height, Arrays.copyOf(contents, contents.length));
    }

    public int[] getCopyOfContents() {
        return Arrays.copyOf(contents, contents.length);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isDifferent(int[] modifiedContents) {
        return !Arrays.equals(contents, modifiedContents);
    }

    public boolean isWon() {
        return Arrays.stream(contents).anyMatch(element -> element == WINNING_VALUE);
    }

    public boolean isTerminal() {
        return allApplicableActions().isEmpty();
    }

    public List<Action> allApplicableActions() {
        if (isWon()) {
            return ImmutableList.of();
        }
        ImmutableList.Builder<Action> builder = ImmutableList.builder();
        for (BoardAction action : ALL_ACTIONS) {
            if (isDifferent(action.slide(this))) {
                builder.add(action);
            }
        }
        return builder.build();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                result.append(contents[x + y * width]);
                result.append(x == width - 1 ? "\n" : ", ");
            }
        }
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardState that = (BoardState) o;

        return width == that.width && height == that.height && Arrays.equals(contents, that.contents);
    }

    @Override
    public int hashCode() {
        int result = width;
        result = 31 * result + height;
        result = 31 * result + Arrays.hashCode(contents);
        return result;
    }
}
