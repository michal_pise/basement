package net.rule126.twentyfortyeight;

import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.state.State;

public class BoardTerminalFunction implements TerminalFunction {
    @Override
    public boolean isTerminal(State state) {
        return ((BoardState) state).isTerminal();
    }
}
